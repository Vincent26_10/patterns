import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ReplaceTheForZe11 {
	private static String nameOfFile;
	public static final String TPM = "temporal";
    private static File creator;
    private static File f;
    
    private static void fileCreator(String nameOfFileCreator) throws IOException {
        nameOfFile = nameOfFileCreator;
        f = new File(TPM);
        creator = new File(nameOfFile);
        if (!creator.exists()) {
            creator.createNewFile();
        }
    }
    
    public static void replaceThe(String FileReplace) throws IOException {
    	fileCreator(FileReplace);
    	BufferedReader in = null;
    	PrintWriter out = null;
    	boolean found = false; 
    	
    	try {
    		in = new BufferedReader(new FileReader(nameOfFile));
    		out = new PrintWriter(new FileWriter(f));
    		Pattern p = Pattern.compile("[Tt]he");
    		String s = null;
    		while((s=in.readLine()) != null) {
    			 Matcher matcher = p.matcher(s);
    			 
    			 while(matcher.find()) {
    				 out.print("ze ");
    				 found = true;
    			 }
    			 if(!found) {
    				 out.print(s);
    				 found = false;
    			 }
    			 
    		}
    		
    		f.renameTo(new File(nameOfFile));
    	}finally {
    		 if (in != null) {
                 in.close();
             }
    		 if (out != null) {
                 out.close();
             }
    	}
    }
	
	public static void main(String[] args) throws IOException {
		replaceThe("a");
	}

}
