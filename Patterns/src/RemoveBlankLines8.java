import java.io.*;
import java.util.*;

public class RemoveBlankLines8 {

	private static String nameOfFile;
	public static final String TMP = "temporal";
	private static File f;
	private static File creator;

	public static void main(String[] args) throws IOException {

		removeBlankLines(nameOfFile);
	}

	private static void fileCreator(String nameOfFileCreator) throws IOException {
		nameOfFile = nameOfFileCreator;
		f = new File(TMP);
		creator = new File(nameOfFile);
		if (!creator.exists()) {
			creator.createNewFile();
		}
	}

	private static void removeBlankLines(String nameOfFileToRemove) throws IOException {
		fileCreator(nameOfFileToRemove);
		BufferedReader in = null;
		PrintWriter out = null;
		try {
			in = new BufferedReader(new FileReader(nameOfFile));
			out = new PrintWriter(new FileWriter(f));
			String string = null;
			while ((string = in.readLine()) != null) {
				if((string.matches("\\S*"))) {
					out.println(string);
				}
			}
			f.renameTo(new File(nameOfFile));
		} finally {
			if (in != null) {
				in.close();
			}
			if (out != null) {
				out.close();
			}
		}
	}

}