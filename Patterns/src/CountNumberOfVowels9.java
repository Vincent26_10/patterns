import java.io.*;
import java.util.regex.*;

public class CountNumberOfVowels9 {

    private static String nameOfFile;
    private static File creator;
    private static int numVowels = 0;

    public static void main(String[] args) throws IOException {
        countNumberOfVocals("a");
    }

   private static void fileCreator(String nameOfFileCreator) throws IOException {
        nameOfFile = nameOfFileCreator;
        creator = new File(nameOfFile);
        if (!creator.exists()) {
            creator.createNewFile();
        }
    }

    public static void countNumberOfVocals(String nameOfFileToCount) throws IOException {
       fileCreator(nameOfFileToCount);
        BufferedReader in = null;
        try {
            in = new BufferedReader(new FileReader(nameOfFile));
            Pattern p = Pattern.compile("[aeiouAEIOU]");
            String string = null;
            while ((string = in.readLine()) != null) {
                Matcher matcher = p.matcher(string);
                while(matcher.find()) {
                    numVowels++;
                }
            }
            System.out.println(getNumVowels());
        } finally {
            if (in != null) {
                in.close();
            }
        }
    }

    public static int getNumVowels() {
        return numVowels;
    }

}