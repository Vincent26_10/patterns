
public class StringMatcher {

	public static void main(String[] args) {

	}

	// returns true if the string matches exactly "true"
	public static boolean isTrue(String s) {
		return s.matches("true");
	}

	// returns true if the string matches exactly "true" or "True"
	public static boolean isTrueVersion2(String s) {
		return s.matches("[tT]rue");
	}
	//1
	public static boolean isTrueOrYes(String s) {
		return s.matches("[tT]rue|[yY]es");
	}
	//2
	public static boolean containsTrue(String s) {
		return s.matches("(true)+");
	}
	//3
	public static boolean isThreeLetters(String s) {
		return s.matches("[a-zA-Z]{3}");
	}
	//4
	public static boolean isNoNumberAtBeginning(String s) {
		return s.matches("^[^\\d].*");
	}
	//5
	public static boolean isIntersection(String s) {
		return s.matches("[\\w&&[^b]]*");
	}
	//6
	public static boolean isLessThanThreeHundret(String s) {
		return s.matches("[\\D]*[12]?[0-9]{1,2}[\\D]*");
	}
	//7
	public static boolean validIP(String s) {
		return s.matches("[\\D]*((25[0-5]|2[0-4]\\d|[0-1]?\\d\\d?)\\.){3}(25[0-5]|2[0-4]\\d|[0-1]?\\d\\d?)[\\D]*");
	}
}
